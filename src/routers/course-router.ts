import BaseRouter from '@/routers/base-router';
import {
  GetCourseController,
  CreateCourseController,
  DeleteCourseController,
  GetCourseByIdController,
  UpdateCourseController,
} from '@/controllers/course';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';

class CourseRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetCourseController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateCourseController);
    this.router.get('/:id', GetCourseByIdController);
    this.router.put('/:id', VerifyToken, Permission({ roles: ['admin'] }), UpdateCourseController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteCourseController);
  }
}

export default new CourseRoutes().router;
