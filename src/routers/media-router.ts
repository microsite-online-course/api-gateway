import BaseRouter from '@/routers/base-router';
import { GetMediaController, CreateMediaController, DeleteMediaController } from '@/controllers/media';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';

class MediaRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetMediaController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateMediaController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteMediaController);
  }
}

export default new MediaRoutes().router;
