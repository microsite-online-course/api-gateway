import BaseRouter from '@/routers/base-router';
import {
  GetChapterController,
  CreateChapterController,
  DeleteChapterController,
  GetChapterByIdController,
  UpdateChapterController,
} from '@/controllers/chapter';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';
class ChapterRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetChapterController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateChapterController);
    this.router.get('/:id', GetChapterByIdController);
    this.router.put('/:id', VerifyToken, Permission({ roles: ['admin'] }), UpdateChapterController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteChapterController);
  }
}

export default new ChapterRoutes().router;
