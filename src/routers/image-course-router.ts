import BaseRouter from '@/routers/base-router';
import {
  GetImageCourseController,
  CreateImageCourseController,
  DeleteImageCourseController,
  GetImageCourseByIdController,
  UpdateImageCourseController,
} from '@/controllers/image-course';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';

class ImageCourse extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetImageCourseController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateImageCourseController);
    this.router.get('/:id', GetImageCourseByIdController);
    this.router.put('/:id', VerifyToken, Permission({ roles: ['admin'] }), UpdateImageCourseController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteImageCourseController);
  }
}

export default new ImageCourse().router;
