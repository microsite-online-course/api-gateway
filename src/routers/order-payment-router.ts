import BaseRouter from '@/routers/base-router';
import { GetOrderPaymentController } from '@/controllers/order-payment';
import VerifyToken from '@/middleware/verify-token';

class OrderPaymentRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', VerifyToken, GetOrderPaymentController);
  }
}

export default new OrderPaymentRoutes().router;
