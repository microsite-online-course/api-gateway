import BaseRouter from '@/routers/base-router';
import { RegisterUserController, LoginUserController, UpdateUserController, GetUserController, LogoutUserController } from '@/controllers/users';
import VerifyToken from '@/middleware/verify-token';

class UserRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/show', VerifyToken, GetUserController);
    this.router.post('/logout', VerifyToken, LogoutUserController);
    this.router.post('/login', LoginUserController);
    this.router.post('/register', RegisterUserController);
    this.router.put('/update', VerifyToken, UpdateUserController);
  }
}

export default new UserRoutes().router;
