import BaseRouter from '@/routers/base-router';
import {
  GetReviewController,
  CreateReviewController,
  DeleteReviewController,
  GetReviewByIdController,
  UpdateReviewController,
} from '@/controllers/review';
import VerifyToken from '@/middleware/verify-token';

class Review extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetReviewController);
    this.router.post('/', VerifyToken, CreateReviewController);
    this.router.get('/:id', GetReviewByIdController);
    this.router.put('/:id', VerifyToken, UpdateReviewController);
    this.router.delete('/:id', VerifyToken, DeleteReviewController);
  }
}

export default new Review().router;
