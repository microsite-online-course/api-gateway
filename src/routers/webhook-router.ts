import BaseRouter from '@/routers/base-router';
import { CreateWebhookController } from '@/controllers/webhook';

class WebhookRoutes extends BaseRouter {
  public routes(): void {
    this.router.post('/', CreateWebhookController);
  }
}

export default new WebhookRoutes().router;
