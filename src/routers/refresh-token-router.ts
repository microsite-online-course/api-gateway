import BaseRouter from '@/routers/base-router';
import { GetRefreshTokenController } from '@/controllers/refresh-tokens';

class RefreshTokenRoutes extends BaseRouter {
  public routes(): void {
    this.router.post('/', GetRefreshTokenController);
  }
}

export default new RefreshTokenRoutes().router;
