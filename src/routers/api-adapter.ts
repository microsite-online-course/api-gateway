import axios from 'axios';
require('dotenv').config();

const apiAdapter = (baseUrl: string) => {
  return axios.create({
    baseURL: baseUrl,
    timeout: (process.env.TIMEOUT || 5000) as number, // jika 5 detik tidak ada response maka akan ada error return
  });
};

export default apiAdapter;
