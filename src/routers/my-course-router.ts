import BaseRouter from '@/routers/base-router';
import {
  GetMyCourseController,
  CreateMyCourseController,
  DeleteMyCourseController,
  GetMyCourseByIdController,
  UpdateMyCourseController,
} from '@/controllers/my-course';
import VerifyToken from '@/middleware/verify-token';

class MyCourse extends BaseRouter {
  public routes(): void {
    this.router.get('/', VerifyToken, GetMyCourseController);
    this.router.post('/', VerifyToken, CreateMyCourseController);
    this.router.get('/:id', VerifyToken, GetMyCourseByIdController);
    this.router.put('/:id', VerifyToken, UpdateMyCourseController);
    this.router.delete('/:id', VerifyToken, DeleteMyCourseController);
  }
}

export default new MyCourse().router;
