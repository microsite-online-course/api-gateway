import BaseRouter from '@/routers/base-router';
import {
  GetLessonController,
  CreateLessonController,
  DeleteLessonController,
  GetLessonByIdController,
  UpdateLessonController,
} from '@/controllers/lesson';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';

class LessonRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetLessonController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateLessonController);
    this.router.get('/:id', GetLessonByIdController);
    this.router.put('/:id', VerifyToken, Permission({ roles: ['admin'] }), UpdateLessonController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteLessonController);
  }
}

export default new LessonRoutes().router;
