import BaseRouter from '@/routers/base-router';
import {
  GetMentorController,
  CreateMentorController,
  GetMentorByIdController,
  DeleteMentorController,
  UpdateMentorController,
} from '@/controllers/mentor';
import VerifyToken from '@/middleware/verify-token';
import Permission from '@/middleware/permission';

class MentorRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', VerifyToken, Permission({ roles: ['admin'] }), GetMentorController);
    this.router.get('/:id', VerifyToken, Permission({ roles: ['admin'] }), GetMentorByIdController);
    this.router.post('/', VerifyToken, Permission({ roles: ['admin'] }), CreateMentorController);
    this.router.put('/:id', VerifyToken, Permission({ roles: ['admin'] }), UpdateMentorController);
    this.router.delete('/:id', VerifyToken, Permission({ roles: ['admin'] }), DeleteMentorController);
  }
}

export default new MentorRoutes().router;
