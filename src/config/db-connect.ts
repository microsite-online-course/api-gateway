import { Sequelize } from 'sequelize';
const retry = require('retry');
import dotenv from 'dotenv';

dotenv.config();

const DB_NAME = process.env.DB_NAME as string;
const DB_HOST = process.env.DB_HOST;
const DB_USERNAME = process.env.DB_USERNAME as string;
const DB_PASSWORD = process.env.DB_PASSWORD;

const sequelizeConnection = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
  host: DB_HOST,
  dialect: 'postgres',
});

const operation = retry.operation({
  retries: 5, // maximum number of retries
  factor: 3, // exponential backoff factor
  minTimeout: 1000, // minimum time to wait between retries (in ms)
  maxTimeout: 5000, // maximum time to wait between retries (in ms)
});

operation.attempt((currentAttempt: any) => {
  sequelizeConnection
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      console.error(`Attempt ${currentAttempt} failed: ${err.message}`);
      if (operation.retry(err)) {
        return;
      }
      console.error('Maximum retries exceeded.');
      process.exit(1);
    });
});

process.on('SIGINT', () => {
  sequelizeConnection.close().then(() => {
    console.log('Connection has been closed.');
    process.exit();
  });
});

export default sequelizeConnection;
