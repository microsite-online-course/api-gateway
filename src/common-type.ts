export interface CommonListResponse<T> {
  status: string;
  data: T[];
}
export interface AxiosErrorInterface {
  message: string;
  response: {
    status: number;
    data: {
      message: string;
    };
  };
}

export type CommonErrorResponse = {
  status?: number;
  message?: string;
};
