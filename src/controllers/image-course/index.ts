import GetImageCourseController from '@/controllers/image-course/get-image-course-controller';
import CreateImageCourseController from '@/controllers/image-course/create-image-course-controller';
import DeleteImageCourseController from '@/controllers/image-course/delete-image-course-controller';
import UpdateImageCourseController from '@/controllers/image-course/update-image-course-controller';
import GetImageCourseByIdController from '@/controllers/image-course/get-image-course-by-id-controller';

export {
  GetImageCourseController,
  CreateImageCourseController,
  DeleteImageCourseController,
  UpdateImageCourseController,
  GetImageCourseByIdController,
};
