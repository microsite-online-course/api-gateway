import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_COURSE_ENDPOINT = process.env.SERVICE_COURSE_ENDPOINT as string;

const DeleteLessonController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const id = req.params.id;
    const api = apiAdapter(SERVICE_COURSE_ENDPOINT);
    const lesson = await api.delete(`/api/v1/lesson/delete/${id}`);

    return res.json({
      status: 'success',
      data: lesson.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service lesson unavailable',
    });
  }
};

export default DeleteLessonController;
