import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_COURSE_ENDPOINT = process.env.SERVICE_COURSE_ENDPOINT as string;

const GetChapterByIdController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_COURSE_ENDPOINT);
    const { data: chapter } = await api.get(`/api/v1/chapter/show/${req.params.id}`);

    return res.json({
      status: 'success',
      data: chapter.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service chapter unavailable',
    });
  }
};

export default GetChapterByIdController;
