import GetChapterController from '@/controllers/chapter/get-chapter-controller';
import CreateChapterController from '@/controllers/chapter/create-chapter-controller';
import DeleteChapterController from '@/controllers/chapter/delete-chapter-controller';
import UpdateChapterController from '@/controllers/chapter/update-chapter-controller';
import GetChapterByIdController from '@/controllers/chapter/get-chapter-by-id-controller';

export { GetChapterController, CreateChapterController, DeleteChapterController, UpdateChapterController, GetChapterByIdController };
