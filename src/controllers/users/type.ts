export type UserResponseData = {
  uuid?: string;
  name?: string;
  profession?: string;
  email?: string;
  password?: string;
  role?: string;
  avatar?: string | undefined;
  createdAt?: string;
  updatedAt?: string;
};

export interface UserPayload {
  uuid: string | undefined;
  name: string | undefined;
  profession: string | undefined;
  email: string | undefined;
  role: string | undefined;
}

export interface UserResponse<T> {
  status: string;
  data: T;
}

export type GetUserResponse = UserResponse<UserResponseData>;
