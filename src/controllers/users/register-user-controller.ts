import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_USER_ENDPOINT = process.env.SERVICE_USER_ENDPOINT as string;

const RegisterUserController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_USER_ENDPOINT);
    const user = await api.post('/api/v1/user/register', req.body);

    return res.json({
      status: 'success',
      data: user.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service user unavailable',
    });
  }
};

export default RegisterUserController;
