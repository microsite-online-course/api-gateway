import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import jwt, { Secret } from 'jsonwebtoken';
import { GetUserResponse, UserPayload } from '@/controllers/users/type';
import { CommonErrorResponse } from 'common-type';
const SERVICE_USER_ENDPOINT = process.env.SERVICE_USER_ENDPOINT as string;

const LoginUserController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_USER_ENDPOINT);
    const accessTokenSecret: Secret = process.env.JWT_SECRET as string;
    const refreshTokenSecret: Secret = process.env.JWT_SECRET_REFRESH_TOKEN as string;
    const body = req.body;

    const { data: user } = await api.post<GetUserResponse>('/api/v1/user/login', body);

    const userPayload: UserPayload = {
      uuid: user.data.uuid,
      name: user.data.name,
      profession: user.data.profession,
      email: user.data.email,
      role: user.data.role,
    };

    const token = jwt.sign({ data: userPayload }, accessTokenSecret, {
      expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRED as string,
    });

    const refreshToken = jwt.sign({ data: userPayload }, refreshTokenSecret, {
      expiresIn: process.env.JWT_REFRESH_ACCESS_TOKEN_EXPIRED as string,
    });

    await api.post('/api/v1/refresh-token', { refresh_token: refreshToken, user_uuid: user.data.uuid });

    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      //secure: true,
      sameSite: 'none',
      maxAge: (process.env.JWT_REFRESH_ACCESS_TOKEN_EXPIRED || 86400000) as number, // ini jadinya 1 hari,
    });

    return res.json({
      status: 'success',
      data: {
        user: userPayload,
        token: token,
        //refreshToken: refreshToken,
      },
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service user unavailable',
    });
  }
};

export default LoginUserController;
