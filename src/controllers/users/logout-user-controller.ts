import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { CommonErrorResponse, AxiosErrorInterface } from 'common-type';
import { AxiosError } from 'axios';
const SERVICE_USER_ENDPOINT = process.env.SERVICE_USER_ENDPOINT as string;

const LogoutUserController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_USER_ENDPOINT);
    const user = req.user;
    const { data } = await api.post('/api/v1/user/logout', { uuid: user?.uuid ?? 0 });

    return res.json({
      status: 'success',
      message: data.message,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;
    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service user unavailable',
    });
  }
};

export default LogoutUserController;
