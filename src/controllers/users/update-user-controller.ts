import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { GetUserResponse } from '@/controllers/users/type';
import { CommonErrorResponse } from 'common-type';

const SERVICE_USER_ENDPOINT = process.env.SERVICE_USER_ENDPOINT as string;

const UpdateUserController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const user = req.user;
    const body = req.body;
    const api = apiAdapter(SERVICE_USER_ENDPOINT);
    const { data: updateUser } = await api.put<GetUserResponse>(`/api/v1/user/update/${user?.uuid}`, body);

    return res.json({
      status: 'success',
      data: updateUser.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service user unavailable',
    });
  }
};

export default UpdateUserController;
