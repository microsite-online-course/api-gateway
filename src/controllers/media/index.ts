import GetMediaController from '@/controllers/media/get-media-controller';
import CreateMediaController from '@/controllers/media/create-media-controller';
import DeleteMediaController from '@/controllers/media/delete-media-controller';

export { GetMediaController, CreateMediaController, DeleteMediaController };
