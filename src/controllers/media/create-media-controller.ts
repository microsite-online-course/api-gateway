import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_MEDIA_ENDPOINT = process.env.SERVICE_MEDIA_ENDPOINT as string;

const CreateMediaController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_MEDIA_ENDPOINT);
    const { data: media } = await api.post('/api/v1/media', req.body);

    return res.json({
      status: 'success',
      data: media.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service media unavailable',
    });
  }
};

export default CreateMediaController;
