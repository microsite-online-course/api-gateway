import GetCourseController from '@/controllers/course/get-course-controller';
import CreateCourseController from '@/controllers/course/create-course-controller';
import DeleteCourseController from '@/controllers/course/delete-course-controller';
import UpdateCourseController from '@/controllers/course/update-course-controller';
import GetCourseByIdController from '@/controllers/course/get-course-by-id-controller';

export { GetCourseController, CreateCourseController, DeleteCourseController, UpdateCourseController, GetCourseByIdController };
