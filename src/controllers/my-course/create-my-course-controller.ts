import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_COURSE_ENDPOINT = process.env.SERVICE_COURSE_ENDPOINT as string;

const CreateMyCourseController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const body = {
      course_uuid: req.body.course_uuid,
      user_uuid: req.user?.uuid,
    };
    const api = apiAdapter(SERVICE_COURSE_ENDPOINT);
    const { data: myCourse } = await api.post('/api/v1/my-course', body);

    return res.json({
      status: 'success',
      data: myCourse.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service my course unavailable',
    });
  }
};

export default CreateMyCourseController;
