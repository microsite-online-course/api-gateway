import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_ORDER_ENDPOINT = process.env.SERVICE_ORDER_ENDPOINT as string;

const CreateWebhookController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const api = apiAdapter(SERVICE_ORDER_ENDPOINT);
    const { data: webhook } = await api.post('/api/v1/webhook', req.body);

    return res.json({
      status: 'success',
      data: webhook.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service webhook unavailable',
    });
  }
};

export default CreateWebhookController;
