import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import jwt, { Secret, VerifyErrors, JwtPayload } from 'jsonwebtoken';
import { UserPayload } from '@/controllers/users/type';
import { CommonErrorResponse } from 'common-type';
import cookie from 'cookie';

const SERVICE_USER_ENDPOINT = process.env.SERVICE_USER_ENDPOINT as string;

const GetRefreshTokenController = async (req: Request, res: Response) => {
  try {
    const api = apiAdapter(SERVICE_USER_ENDPOINT);
    const cookies = req.headers.cookie;
    const accessTokenSecret: Secret = process.env.JWT_SECRET as string;
    const refreshTokenSecret: Secret = process.env.JWT_SECRET_REFRESH_TOKEN as string;
    const body = req.body;
    const userUuid = body.user_uuid;

    const parsedCookies = cookie.parse(cookies || '');

    if (!parsedCookies?.refreshToken)
      return res.status(401).json({
        status: 'error',
        message: 'unauthorized',
      });

    const refreshToken = parsedCookies?.refreshToken;

    await api.get('/api/v1/refresh-token', { params: { refreshToken: refreshToken } });

    jwt.verify(refreshToken, refreshTokenSecret, { ignoreExpiration: true }, (err: VerifyErrors | null, decoded: string | JwtPayload | undefined) => {
      const userPayload = (decoded as JwtPayload)?.data as UserPayload;
      if (err) {
        return res.status(403).json({
          status: 'error',
          message: err.message,
        });
      }

      if (userUuid !== userPayload.uuid) {
        return res.status(400).json({
          status: 'error',
          message: 'user_uuid is invalid',
        });
      }

      const newToken = jwt.sign({ data: userPayload }, accessTokenSecret, {
        expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRED as string,
      });

      return res.json({
        status: 'success',
        data: {
          token: newToken,
        },
      });
    });
  } catch (error) {
    const errorResponse = error as AxiosError;

    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service user unavailable',
    });
  }
};

export default GetRefreshTokenController;
