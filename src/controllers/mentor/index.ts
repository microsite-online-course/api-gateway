import GetMentorController from '@/controllers/mentor/get-mentor-controller';
import GetMentorByIdController from '@/controllers/mentor/get-mentor-by-id-controller';
import CreateMentorController from '@/controllers/mentor/create-mentor-controller';
import DeleteMentorController from '@/controllers/mentor/delete-mentor-controller';
import UpdateMentorController from '@/controllers/mentor/update-mentor-controller';

export { GetMentorController, GetMentorByIdController, CreateMentorController, DeleteMentorController, UpdateMentorController };
