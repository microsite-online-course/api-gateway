import { Request, Response } from 'express';
import apiAdapter from '@/routers/api-adapter';
import { AxiosError } from 'axios';
import { CommonErrorResponse } from 'common-type';
const SERVICE_COURSE_ENDPOINT = process.env.SERVICE_COURSE_ENDPOINT as string;

const GetReviewController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const page = req.query.page;
    const courseUuid = req.query.course_uuid;

    const api = apiAdapter(SERVICE_COURSE_ENDPOINT);
    const { data: review } = await api.get('/api/v1/review', { params: { page, course_uuid: courseUuid } });

    return res.json({
      status: 'success',
      metadata: review.metadata,
      data: review.data,
    });
  } catch (error) {
    const errorResponse = error as AxiosError;
    if (errorResponse.response) {
      const { status, data } = errorResponse.response;
      const dataError = data as CommonErrorResponse;

      return res.status(status).json({
        status: 'error',
        message: dataError.message,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'service review unavailable',
    });
  }
};

export default GetReviewController;
