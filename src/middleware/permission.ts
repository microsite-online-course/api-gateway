import { Request, Response, NextFunction } from 'express';

type PermissionProps = {
  roles: string[];
};

const Permission = ({ roles }: PermissionProps): any => {
  return (req: Request, res: Response, next: NextFunction) => {
    const role = req.user?.role as string;
    if (!roles.includes(role)) {
      return res.status(405).json({
        status: 'error',
        message: `you don't have permission`,
      });
    }

    return next();
  };
};

export default Permission;
