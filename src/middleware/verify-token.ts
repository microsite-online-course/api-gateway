import jwt, { Secret, VerifyErrors, JwtPayload } from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import { UserPayload } from '@/controllers/users/type';

declare global {
  namespace Express {
    interface Request {
      user?: UserPayload;
    }
  }
}

const VerifyToken = (req: Request, res: Response, next: NextFunction): any => {
  const authHeader = req.headers.authorization as string;
  const accessTokenSecret: Secret = process.env.JWT_SECRET as string;

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    return res.status(401).json({
      status: 'error',
      message: 'Authorization header missing or invalid',
    });
  }

  const token = authHeader.slice(7);

  jwt.verify(token, accessTokenSecret, (err: VerifyErrors | null, decoded: string | JwtPayload | undefined) => {
    if (err) {
      return res.status(403).json({
        status: 'error',
        message: err.message,
      });
    }

    req.user = (decoded as JwtPayload).data as UserPayload;
    return next();
  });
};

export default VerifyToken;
