import 'module-alias/register';
import express, { Application, Request, Response } from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';
import { config } from 'dotenv';
import mediaRouter from '@/routers/media-router';
import userRouter from '@/routers/user-router';
import mentorRouter from '@/routers/mentor-router';
import courseRouter from '@/routers/course-router';
import refreshTokenRouter from '@/routers/refresh-token-router';
import chapterRouter from '@/routers/chapter-router';
import lessonRouter from '@/routers/lesson-router';
import imageCourse from '@/routers/image-course-router';
import myCourseRouter from '@/routers/my-course-router';
import reviewRouter from '@/routers/review-router';
import webhookRouter from '@/routers/webhook-router';
import orderPaymentRouter from '@/routers/order-payment-router';
class App {
  public app: Application;

  constructor() {
    this.app = express();
    this.plugins();
    this.routes();
    config();
  }

  protected plugins(): void {
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
    this.app.use(morgan('dev'));
    this.app.use(compression());
    this.app.use(helmet());
    this.app.use(cors());
  }

  protected routes(): void {
    this.app.use('/api/media', mediaRouter);
    this.app.use('/api/user', userRouter);
    this.app.use('/api/mentor', mentorRouter);
    this.app.use('/api/course', courseRouter);
    this.app.use('/api/chapter', chapterRouter);
    this.app.use('/api/lesson', lessonRouter);
    this.app.use('/api/image-course', imageCourse);
    this.app.use('/api/my-course', myCourseRouter);
    this.app.use('/api/review', reviewRouter);
    this.app.use('/api/webhook', webhookRouter);
    this.app.use('/api/refresh-token', refreshTokenRouter);
    this.app.use('/api/order-payment', orderPaymentRouter);
  }
}

const app = new App().app;
const port: number = (process.env.PORT || 8081) as number;
app.listen(port);
