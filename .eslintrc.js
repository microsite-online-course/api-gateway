module.export = {
    env: {
        es6: true,
        node: true,
    },
    parser: '@typescript-eslint/parser',
    extends: [
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
        'plugin:@typescript-eslint/recommended',
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
    ],
    parserOptions: {
        ecmaVersion: '2017',
        sourceType: 'module',
    },
    plugins: ['@typescript-eslint', 'prettier'],
    rules: {
        'linebreak-style': ['error', 'unix'],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        '@typescript-eslint/no-explicit-any': 'error',
        'no-console': ['error', { allow: ['warn', 'error'] }],
        'lines-between-class-members': ['error', 'always'],
        '@typescript-eslint/no-unused-vars': [
            'error',
            {
                argsIgnorePattern: '^_',
                varsIgnorePattern: '^_',
                caughtErrorsIgnorePattern: '^_',
            },
        ],
        'no-loop-func': 'error',
        'max-params': ['error', 3],
        'max-depth': ['error', 3],
        complexity: ['error', 6],
        'no-nested-ternary': 'error',
        'max-lines': ['error', 250],
        'import/order': [
            'error',
            {
                groups: [
                    'builtin',
                    'external',
                    'parent',
                    'sibling',
                    'index',
                    'object',
                    'type',
                ],
                pathGroups: [
                    {
                        pattern: '@/**/**',
                        group: 'parent',
                        position: 'before',
                    },
                ],
                alphabetize: { order: 'asc' },
            },
        ],
        'no-restricted-imports': [
            'error',
            {
                patterns: ['../'],
            },
        ],
        '@typescript-eslint/naming-convention': [
            'error',
            { selector: 'typeLike', format: ['PascalCase'] },
        ],
        'check-file/filename-naming-convention': [
            'error',
            { 'src/**/*': 'KEBAB_CASE' },
            {
                ignoreMiddleExtensions: true,
            },
        ],
        'check-file/folder-naming-convention': [
            'error',
            {
                'src/**/*': 'KEBAB_CASE',
            },
        ],
        'padding-line-between-statements': [
            'error',
            { blankLine: 'always', prev: ['const', 'let'], next: '*' },
            {
                blankLine: 'any',
                prev: ['const', 'let'],
                next: ['const', 'let'],
            },
            { blankLine: 'always', prev: 'directive', next: '*' },
            { blankLine: 'any', prev: 'directive', next: 'directive' },
            { blankLine: 'always', prev: ['case', 'default'], next: '*' },
        ],
        'newline-before-return': 'error',
    },
    ignorePatterns: ['**/*.js', 'build/*'],
};
